#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Version:
# - 0.2
#
# Copyright 2012 Gianguido Sorà This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This program is
# distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the General Public License for more details. You should have received a copy of the GNU General Public License along
# with this program. If not, see http://www.gnu.org/licenses/.
#
#

import mechanize
import cookielib
import html2text
import sys
import os
from regexprint import tatregex

if len(sys.argv)<2:
	print 'Please provide one tracking code'
	exit()

# Mechanize browser istance
browser = mechanize.Browser()

# Cookie jar. Mhmmm... Cookies...
cookies = cookielib.LWPCookieJar()
browser.set_cookiejar(cookies)

# Browser options
browser.set_handle_equiv(True)
browser.set_handle_redirect(True)
browser.set_handle_referer(True)
browser.set_handle_robots(False)

# refresh tweak
browser.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)

# set the user agent
browser.addheaders = [('User-agent', 'Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20100101 Firefox/17.0')]

# Open the TNT tracking site
tntsite = browser.open('http://www.tnt.it/tracking/Tracking.do')
html = tntsite.read()

# select the 'trackingForm'
browser.select_form("trackingForm")

# select item '1' from the radiobox
browser.form['wt'] = ['1']

# input the tracking code got from sys.argv[1]
browser['consigNos'] = '%s' % (sys.argv[1])

# emulate 'ENTER' button
result = browser.submit()

# put the result page (raw HTML) into the variable 'rawhtml'
rawhtml = result.read()

# convert the raw HTML into ASCII text and save into .status, do the magic regex then rm it
status = open('./.status', 'w')
status.write(html2text.html2text(rawhtml))
status.close()
print '\n'
tatregex()
print '\n'
os.remove('./.status')
